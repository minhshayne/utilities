<?php get_header(); the_post();?>
<div class="body-wrap">
<article class="container single body">
    <figure class="single-content col-md-8">
    <h1><?php the_title(); ?></h1>
    <div class="content"><?php the_content(); ?></div>
    <div class="fb-cmt"><div class="fb-comments" data-href="<?php the_permalink();?>" data-width="100%" data-numposts="5"></div></div>
    </figure>
    <figure class="widget-control col-md-4">
    <?php get_sidebar(); ?>
    </figure>
</article>
</div>
<?php get_footer(); ?>
