<?php get_header(); the_post(); ?>
<article class="container page-body">
<?php the_content(); ?>
</article>
<?php get_footer(); ?>
