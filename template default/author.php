<?php get_header();
$user_profile = get_user_by('slug', get_query_var('author_name'));
$user_id = $user_profile->ID;
$user = array(
    'display_name'  => $user_profile->display_name,
    'fname'         => $user_profile->user_firstname ,
    'lname'         => $user_profile->user_lastname ,
    'born'          => get_user_meta($user_id,'born',true),
    'sex'           => get_user_meta($user_id,'sex',true),
    'province'      => get_user_meta($user_id,'province',true),
    'hobbies'       => get_user_meta($user_id,'hobbies',true),
    'descr'         => $user_profile->description
);
if (!function_exists(ms_display_info)){
    function ms_display_info($field){
        if($field) return $field;
        return '<span class="not-update">Chưa cập nhật!</span>';
    }
}
?>
<article class="container page-body">
<div class="container title-page">
    <h1>Thông tin thành viên</h1>
    <div class="descr">Thông tin thành viên <?php echo ms_display_info($user['display_name']);?> được phơi bày bao gồm</div>
</div>
<div class="container status-profile">
    <div class="avatar col-md-3"><?php echo get_avatar($user_id, 200); ?></div>
    <div class="descr col-md-9">
        <span class="ques"><h1 class="display_name"><?php echo ms_display_info($user['display_name']); ?></h1>
            <?php echo ms_display_info($user['descr']); ?>
        </span>
    </div>
</div>
<div class="container view-profile-form">
    
    <div class=" col-md-6 clearfix">
    <label class="col-md-4">Tên hiển thị</label>
    <p class="col-md-8 display_field"><?php echo ms_display_info($user['display_name']);?></p>
    </div>
    
    <div class=" col-md-6 clearfix">
    <label class="col-md-4">Họ</label>
    <p class="display_field col-md-8"><?php echo ms_display_info($user['fname']);?></p>
    </div>
    
    <div class="col-md-6 clearfix">
    <label class="col-md-4">Tên</label>
        <p class="display_field col-md-8"><?php echo ms_display_info($user['lname']);?></p>
    </div>
    
    <div class="col-md-6 clearfix">
    <label class="col-md-4">Nơi ở</label>
        <p class="display_field col-md-8"><?php echo ms_display_info($user['province']);?></p>
    </div>
    
    <div class="col-md-6 clearfix">
    <label class="col-md-4">Năm sinh</label>
        <p class="display_field col-md-8"><?php echo ms_display_info($user['born']);?></p>
    </div>
    
    <div class="col-md-6 clearfix">
    <label class="col-md-4">Giới tính</label>
        <p class="display_field col-md-8"><?php echo ms_display_info($user['sex']);?></p>
    </div>
    
    <div class="col-md-12 clearfix">
    <label class="col-md-2">Sở thích</label>
        <p class="display_field col-md-10"><?php echo ms_display_info($user['hobbies']);?></p>
    </div>
    
</div>
</article>
<?php get_footer(); ?>
