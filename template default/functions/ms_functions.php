<?php
/*MS Function Action and Filter*/
function ms_custom_excerpt_length($length) {
    return 200;
}
add_filter('excerpt_length', 'ms_custom_excerpt_length', 999);

function ms_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'ms_excerpt_more');

function remove_wp_version() {
     return '';
}
add_filter('the_generator', 'remove_wp_version');

function ms_admin_bar(){
	return false;
}
add_filter( 'show_admin_bar' , 'ms_admin_bar');

?>