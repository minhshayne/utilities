<footer>
<div class="footer-body container">
    <h2 class="col-md-12">
        <?php echo get_bloginfo();?>
        <i class="fa fa-copyright"></i> Created by minhshayne by wordpress CMS <i class="fa fa-wordpress"></i>
    </h2>
    <div class="social-footer col-md-12">
        <a href="https://www.facebook.com/MinhShayneProduction/" rel="nofollow" target="_blank"><i class="fa fa-facebook"></i></a>
        <a href="https://www.youtube.com/user/nhoxteen1992" rel="nofollow" target="_blank"><i class="fa fa-youtube"></i></a>
        <a href="mailto:rocket.hacker@gmail.com" rel="nofollow" target="_blank"><i class="fa fa-envelope-o"></i></a>
    
    </div>
</div>
<script>
    $(document).ready(function(){$("header .header-wrap").scrollToFixed({marginTop:0,zIndex:2});$("html").niceScroll();
    $('.toggle-mobile').click(function(){$('.wrap-menu-mobile').toggle();});    
                            });
</script>
    <!-- GOOGLE ANALYTICS -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-73882043-1', 'auto'); ga('send', 'pageview');

</script>
    <!-- END GOOGLE ANALYTICS -->
</footer>

<?php wp_footer(); ?>
</body>
</html>
