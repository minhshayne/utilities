<?php get_header(); ?>
<div class="body-wrap">
<article class="container cat body">
<div class="col-md-8 content-col">
<?php 
    while(have_posts()) : the_post(); 
        get_template_part('post','block');
    endwhile;
?>
<section class="page-nav-search col-md-12"><?php echo paginate_links(array('prev_text'=>'<i class="fa fa-angle-left"></i>','next_text'=>'<i class="fa fa-angle-right"></i>')); ?>
</div>
<div class="col-md-4 widget-col"><?php dynamic_sidebar('sidebar-2');?></div>
</section>
    
</article>
</div>
<?php get_footer(); ?>
