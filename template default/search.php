<?php get_header(); ?>
<div class="body-wrap">
<article class="container search body">
<?php 
    if(have_posts()) { ?>
    <div class="col-md-8 content-col">
    <?php
    while(have_posts()) : the_post(); 
        get_template_part('post','block');
    endwhile; ?>
    
        <section class="page-nav-search col-md-12"><?php echo paginate_links(array('prev_text'=>'<i class="fa fa-angle-left"></i>','next_text'=>'<i class="fa fa-angle-right"></i>')); ?>
        </section>
    </div>
    <div class="col-md-4 widget-col"><?php get_sidebar();?></div>
    <?php } else {?>
    
    <div class="not-found">
    <h1>NOT FOUND</h1>
    <h2>Không tìm thấy từ khóa <?php echo $s; ?></h2>
    <span class="descr">TỪ KHÓA BẠN TÌM HIỆN KHÔNG CÓ TRONG HỆ THỐNG! TÌM LẠI TỪ KHÁC HOẶC VỀ</span>
        <a href="<?php echo get_home_url();?>">TRANG CHỦ</a>
    </div>
    <?php }
    
?>
</article>
</div>
<?php get_footer(); ?>
