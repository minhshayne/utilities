<?php
define('WEBRITI_TEMPLATE_DIR_URI', get_template_directory());
require_once(WEBRITI_TEMPLATE_DIR_URI.'/functions/ms_functions.php');

/* THEME FUNCTIONS */
if ( ! function_exists( 'elir_setup' ) ) :
function elir_setup() {
	// load_theme_textdomain( 'elir', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'elir' ),
		'social'  => __( 'Social Links Menu', 'elir' ),
	) );
}
endif; // elir_setup
add_action( 'after_setup_theme', 'elir_setup' );

function ms_login_css() {
wp_enqueue_style('login_css', get_template_directory_uri() . '/login.css' ); // duong dan den file css moi
}
add_action('login_head', 'ms_login_css');


/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function elir_load() {
    wp_enqueue_style( 'boostrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css');
    wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style( 'elir-style', get_stylesheet_uri());
    /* reg script */
    wp_enqueue_script('jQuery2', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js', array(), '2.2');
}
add_action( 'wp_enqueue_scripts', 'elir_load' );
